package wx

import (
	"github.com/astaxie/beego"
)

const (
	MSG_OK  = 0
	MSG_ERR = -1
)

type BaseController struct {
	beego.Controller
}

func (this *BaseController) Prepare() {
	cid, err := this.GetInt64("cid")
	out := make(map[string]interface{})
	out["status"] = MSG_OK
	if err != nil {
		beego.Error("post request company id error is :%s", err.Error())
		out["status"] = MSG_ERR
		out["msg"] = "company is null"
		this.jsonResult(out)
	}
	_, ok := wechatClientMap[cid]
	if !ok {
		beego.Error("company wechat does't exits,company id is ", cid)
		out["status"] = MSG_ERR
		out["msg"] = "company wechat does't exits"
		this.jsonResult(out)
	}
}
func (this *BaseController) jsonResult(out interface{}) {
	this.Data["json"] = out
	this.ServeJSON()
	this.StopRun()
}
