package models

type Menu struct {
	Id        int
	Name      string
	Type      string
	Key       string
	Url       string `orm:"type(text)"`
	IsTop     string
	CompanyId string
}
