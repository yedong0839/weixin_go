package models

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

type TextReply struct {
	Id        int
	Key       string
	MsgType   string
	Content   string `orm:type(text)`
	NewsId    string
	IsDelete  string
	CompanyId string
}

func GetTextReply(textKey string, cid int64) (content string, msgType string) {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.QueryTable("text_reply").Filter("CompanyId", cid).Filter("Key__exact", textKey).Filter("IsDelete", 0).Limit(1).Values(&maps, "MsgType", "Content", "NewsId")
	if err != nil {
		beego.Error("GetTextReply error ", err.Error())
		return "", ""
	}
	if num == 0 {
		return "", ""
	}
	msgTypeR := maps[0]["MsgType"].(string)
	contentR := maps[0]["Content"].(string)
	if msgTypeR == "text" && contentR != "" {
		return contentR, msgTypeR
	} else if msgTypeR == "news" && maps[0]["NewsId"].(string) != "" {
		return maps[0]["NewsId"].(string), msgTypeR
	}
	beego.Error("text key ", textKey, " has no content")
	return "", ""
}
