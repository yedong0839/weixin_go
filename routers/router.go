package routers

import (
	"mywechat/controllers"
	"mywechat/controllers/wx"

	"github.com/astaxie/beego"
)

func init() {
	//	beego.Router("/", &controllers.MainController{})
	beego.Router("/", &controllers.WebController{}, "get:Index")
	beego.Router("/wx/callback", &wx.WXCallackController{})
	beego.Router("/wx/menu/create", &wx.WXController{}, "post:MenuCreate")
	beego.Router("/wx/menu/delete", &wx.WXController{}, "post:MenuDelete")
	beego.Router("/wx/menu/get", &wx.WXController{}, "post:MenuGet")
	beego.Router("/wx/news/add", &wx.WXController{}, "post:AddNews")
	beego.Router("/wx/news/send", &wx.WXController{}, "post:SendGroupNews")
	beego.Router("/wx/news/update", &wx.WXController{}, "post:UpdateNews")
	beego.Router("/wx/news/get", &wx.WXController{}, "post:GetNews")
	beego.Router("/wx/news/privew", &wx.WXController{}, "post:PreviewNews")
	beego.Router("/wx/upload/img", &wx.WXController{}, "post:UploadImage")
	beego.Router("/wx/upload/thumb", &wx.WXController{}, "post:UploadThumb")
	beego.Router("/wx/preview/news", &wx.WXController{}, "post:PreviewNews")
	beego.Router("/wx/media/delete", &wx.WXController{}, "post:DeleteMedia")

	beego.Router("/web/index", &controllers.WebController{}, "get:Index")
	beego.Router("/web/wzg", &controllers.WebController{}, "get:Wzg")
	beego.Router("/web/wfw", &controllers.WebController{}, "get:Wfw")
	beego.Router("/web/detail", &controllers.WebController{}, "get:Detail")
	beego.Router("/web/list", &controllers.WebController{}, "get:List")
}
