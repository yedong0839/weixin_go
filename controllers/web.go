package controllers

import (
	"fmt"
	models "mywechat/models"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

type WebController struct {
	BaseController
}

func (this *WebController) Index() {
	this.Data["pageTitle"] = "微信平台"
	this.display()
}
func (this *WebController) Wzg() {
	this.Data["pageTitle"] = "微组工"
	this.display()
}
func (this *WebController) Wfw() {
	this.Data["pageTitle"] = "微服务"
	this.display()
}

func (this *WebController) Detail() {
	this.Data["pageTitle"] = "微信平台"
	id, error := this.GetInt("id")
	if error != nil {
		this.Ctx.WriteString(error.Error())
		this.StopRun()
	}
	o := orm.NewOrm()
	article := models.Article{Id: id}
	err := o.Read(&article)
	if err != nil {
		this.Ctx.WriteString(err.Error())
		this.StopRun()
	}
	this.Data["pageTitle"] = article.Title
	this.Data["article"] = &article
	this.display()
}

func (this *WebController) List() {
	this.Data["pageTitle"] = "微服务"
	typeA := this.GetString("type")
	typeName := this.GetString("type-name")
	if typeA == "" {
		this.Ctx.WriteString("type 为空")
		this.StopRun()
	}
	if typeName != "" {
		this.Data["pageTitle"] = typeName
	}
	cid, err := this.GetInt64("cid")

	var lists []orm.Params
	o := orm.NewOrm()
	num, err := o.QueryTable("article").Filter("CompanyId", cid).Filter("type__exact", typeA).Values(&lists, "id", "title", "created_at")
	if err != nil {
		this.Ctx.WriteString("error")
		this.StopRun()
	}
	fmt.Printf("Result Nums: %d\n", num)
	if len(lists) == 0 {
		beego.Error("cid ", cid, " article is null ")
		this.Ctx.WriteString("数据为空")
		this.StopRun()
	}
	this.Data["articles"] = &lists
	this.display()
}
