/*
Navicat MySQL Data Transfer

Source Server         : mywechat
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : mywechat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-07-30 11:17:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '文章标题',
  `content` longtext NOT NULL COMMENT '文章内容',
  `created_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `type` varchar(255) NOT NULL COMMENT '文章所属的类别',
  `desc` text NOT NULL COMMENT '文章摘要',
  `map_img` text NOT NULL COMMENT '内容中的图片的本地地址及网址，如{"filepath":"F:/mygo/src/mywechat/material/makefile.png","url":"http://www.img.com/he.png"}]}',
  `event_key` varchar(10) NOT NULL COMMENT '菜单key',
  `pic_url` text NOT NULL COMMENT '封面图片地址',
  `map_wx_img` text NOT NULL,
  `url` text NOT NULL COMMENT '文章对应的网址',
  `is_valid` char(1) NOT NULL DEFAULT '1' COMMENT '是否正在使用 ',
  `order` varchar(2) NOT NULL COMMENT '排序号',
  `is_news` char(1) NOT NULL DEFAULT '0' COMMENT '是否要主动推送',
  `is_menu_news` char(1) NOT NULL DEFAULT '0' COMMENT '是否是菜单点击获取的文章',
  `media_id` varchar(255) NOT NULL COMMENT '所属的图文资源media_id',
  `thumb_path` varchar(255) NOT NULL COMMENT '缩略图本地地址',
  `task_id` varchar(255) NOT NULL COMMENT '消息主动推送的任务ID',
  `is_update` char(1) NOT NULL COMMENT '是否需要更新',
  `company_id` int(11) NOT NULL COMMENT '公司ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '组工简介', '这个很不错哟<img src=\"http://www.baidu.com\" /><img src=\"http://www.baidu2.com\" />', '2016-07-18 17:42:21', '2016-07-18 17:42:26', '', '我看看', '{\"imgs\":[{\"filepath\":\"G:/go/src/mywechat/material/lenovo.png\",\"url\":\"http://www.baidu.com\"},{\"filepath\":\"G:/go/src/mywechat/material/makefile.png\",\"url\":\"http://www.baidu2.com\"}]}', 'wfw', 'http://static.haojieru.com/upload/haojieru/channelicon/91.png', '', 'http://printf.cn/index.php/archives/87/', '1', '0', '1', '0', 'YKJpdCRA6KGpf2APqjUCkqbZaI9KWpD4sXXpFovLAx4', 'G:/go/src/mywechat/material/lenovo.png', '', '0', '1');
INSERT INTO `article` VALUES ('2', '好的呢', '好好学习123', '2016-07-19 09:17:08', '2016-07-19 09:17:11', 'zjkt', '真不错', '', 'wfw', 'http://static.haojieru.com/upload/haojieru/channelicon/360.png', '', 'http://printf.cn/index.php/archives/87/ ', '1', '0', '1', '0', 'YKJpdCRA6KGpf2APqjUCkqbZaI9KWpD4sXXpFovLAx4', 'G:/go/src/mywechat/material/makefile.png', '', '0', '1');

-- ----------------------------
-- Table structure for company_info
-- ----------------------------
DROP TABLE IF EXISTS `company_info`;
CREATE TABLE `company_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '公司名称',
  `wx_app_id` varchar(100) NOT NULL COMMENT '微信app id',
  `wx_app_secret` varchar(100) NOT NULL COMMENT '微信app secret',
  `wx_token` varchar(100) NOT NULL COMMENT '微信 token',
  `wx_ori_id` varchar(100) NOT NULL,
  `wx_encoded_aes_key` varchar(100) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `is_delete` char(1) NOT NULL DEFAULT '0' COMMENT '是否被删除',
  `welcome_text` tinytext NOT NULL COMMENT '订阅时的欢迎词',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company_info
-- ----------------------------
INSERT INTO `company_info` VALUES ('1', 'soft', 'wxa2048be532696550', 'e603f106e9004555de13af52e4dd6e48', 'hihitter', '', '', '2016-07-29 15:33:45', '2016-07-29 15:33:48', '0', '');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '名称',
  `type` varchar(10) NOT NULL COMMENT '类型：click,view',
  `key` varchar(100) NOT NULL COMMENT '菜单对应的key，当类型是click时生效',
  `url` text NOT NULL COMMENT '菜单对应的网址，当类型是view是生效',
  `is_top` char(1) NOT NULL COMMENT '父级菜单的ID',
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '微组工', 'click', 'wzg', '', '0', '1');
INSERT INTO `menu` VALUES ('2', '微服务', 'click', 'wfw', '', '0', '1');
INSERT INTO `menu` VALUES ('3', '微互动', 'view', '', 'http://www.baidu.com', '0', '1');
INSERT INTO `menu` VALUES ('4', '组工官网', 'view', '', 'http://wx.dothing.com', '1', '1');
INSERT INTO `menu` VALUES ('5', '最新公告', 'click', 'zxgg', '', '1', '1');

-- ----------------------------
-- Table structure for text_reply
-- ----------------------------
DROP TABLE IF EXISTS `text_reply`;
CREATE TABLE `text_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(10) NOT NULL COMMENT '输入的文字',
  `msg_type` varchar(10) NOT NULL COMMENT '回复的类型; text，news',
  `content` tinytext NOT NULL COMMENT '回复的内容',
  `news_id` varchar(20) NOT NULL COMMENT '消息ID，形如 3，7，8',
  `is_delete` char(1) NOT NULL DEFAULT '0' COMMENT '是已删除',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `company_id` int(11) NOT NULL COMMENT '公司ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of text_reply
-- ----------------------------
INSERT INTO `text_reply` VALUES ('1', '帮助', 'text', '这里是帮助说明', '', '0', '2016-07-30 07:39:30', '2016-07-30 07:39:35', '1');
