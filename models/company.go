package models

type CompanyInfo struct {
	Id              int
	Name            string
	WxAppId         string
	WxAppSecret     string
	WxToken         string
	WxOriId         string
	WxEncodedAesKey string
	IsDelete        string
	WelcomeText     string `orm:type(text)`
}
